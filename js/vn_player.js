
// Helpers

function loadString(url, success, error)
{
		const data = localStorage.getItem( url );

		if(data) return success(data);

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function()
    {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                if (success)
									localStorage.setItem(url, xhr.responseText);
									success(xhr.responseText);
            } else {
                if (error)
                    console.warn(error);
            }
        }
    };

    xhr.open("GET", url, true);
    xhr.send();
}

function parseXML(xmlStr){

		var dom_parser;

		if (typeof window.DOMParser != "undefined") {

					return ( new window.DOMParser() ).parseFromString(xmlStr, "text/xml");

		} else if (typeof window.ActiveXObject != "undefined" && new window.ActiveXObject("Microsoft.XMLDOM")) {

				var xmlDoc = new window.ActiveXObject("Microsoft.XMLDOM");
				xmlDoc.async = "false";
				xmlDoc.loadXML(xmlStr);

				return xmlDoc;

		} else {

			throw new Error("No XML parser found");

		}
}


// Player

class Player {

    constructor(video_el) {
			this.video = video_el;
			let json_url = this.video.getAttribute("data-src");

			loadString( json_url, (data)=>{  										// загрузка json
				this.onLoadJson( data, (xml_url) =>   						// парсинг json и добавление элементов плеера
				loadString( xml_url,   														// загрузка xml
					this.onLoadXML.bind(this)))});    // парсинг xml и старт плеера


		}
		mouseEvent(event){
			switch (event.type) {
				case "mouseover":
					break;
				case "mouseout":
					break;
				case "click":
					if(this.video.paused){
						this.video.play();
					}else{
						this.video.pause();
					}
					break;
				default:
			}
		}

		videoEvent(event){
			switch (event.type) {
				case "canplay":
					this.video.play()
				break;
				case "playing":
					this.player_div.classList.add("playing")
					break;
				default:
			}
		}

		onLoadXML(data){

			let xml = parseXML(data)

			const media = xml.querySelectorAll("MediaFile")[0];


			const width = media.attributes.width.value;
			const height = media.attributes.height.value;
			const delivery = media.attributes.delivery.value;
			const type = media.attributes.type.value;

			const video = this.video;

			video.src = media.innerHTML.replace("<![CDATA[","").replace("]]>","");
			video.setAttribute("autoplay","");
			video.setAttribute("loop","");
			video.setAttribute("type", type);

			video.autoPlay = true;

			["canplay","emptied","ended","error","playing","progress"].forEach(event=>{
				video.addEventListener(event, this.videoEvent.bind(this));
			});

			this.player_div.children[0].append(video);

			["mouseover", "mouseout", "click"].forEach(event=>{
					video.addEventListener(event, this.mouseEvent.bind(this));
			});

		}

		onLoadJson(data, next){

			data = JSON.parse(data);

			let { width, height, previewImage } = data.formats[0].options;

			let player_div = document.createElement('div');
					player_div.classList = this.video.classList;
					this.video.setAttribute("class", "");
					player_div.innerHTML = `<div style = "padding-bottom: ${ (height/width) * 100 }%;"><img src = "${ previewImage }"/></div>`;

					this.video.parentElement.append(player_div);
												player_div.append(this.video);

					this.player_div = player_div;

					next(data.formats[0].dataUrl);

		}

}

const players = []

document.querySelectorAll("video.vn_player").forEach( player => {
	players.push(new Player(player));
});

// console.log(players);
